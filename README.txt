Purpose 

Enabling this module implements a basic deployment of DAP version 2.0
which meets requirements at http://www.digitalgov.gov/services/dap/analytics-tool-instructions/. 

Installation Instructions

1. Download and enable the module.
2. Add the minified version of the DAP code to your sites/all/libraries/digitalgov_dap folder.
3. Configure the module at /admin/config/system/gov_analytics

Requirements

1. This module requires Libraries 2.x
2. You must install the DAP Analytics library